package pipeline

import (
	"github.com/julienschmidt/httprouter"
)

type Middleware func(httprouter.Handle) httprouter.Handle

type Pipeline struct {
	Middlewares []Middleware
}

func New(middlewares ...Middleware) Pipeline {
	return Pipeline{append(([]Middleware)(nil), middlewares...)}
}

func (pipeline Pipeline) Then(h httprouter.Handle) httprouter.Handle {

	for i := range pipeline.Middlewares {
		h = pipeline.Middlewares[len(pipeline.Middlewares)-1-i](h)
	}

	return h
}

func (pipeline Pipeline) With(middlewares ...Middleware) Pipeline {
	newMiddlewares := make([]Middleware, 0, len(pipeline.Middlewares)+len(middlewares))
	newMiddlewares = append(newMiddlewares, pipeline.Middlewares...)
	newMiddlewares = append(newMiddlewares, middlewares...)

	return Pipeline{newMiddlewares}
}

func (c Pipeline) Extend(Pipeline Pipeline) Pipeline {
	return c.With(Pipeline.Middlewares...)
}